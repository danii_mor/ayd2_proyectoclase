import React from 'react';
import  Carousel  from 'react-bootstrap/Carousel';
import Carousel1 from '../img/echoDot3raGen_GrisOsc.jpg';
import Carousel2 from '../img/echo3raGen_Carbon.jpg';
import Carousel3 from '../img/echoShow5_3raGen_Carbon.jpg';
import '../css/MainContent.css';
class Carrusel extends React.Component {
    render() {
       return (
          <div>
     <Carousel>
              <Carousel.Item>
                
    <img
      className="imgCarousel"
      src={Carousel1}
      alt="First slide"
    />
    <Carousel.Caption>
      <h3 className="CaptionDescription">Amazon Echo Dot (3ra Gen)</h3>
      <p className="CaptionDescription"><b>$49.99</b></p>
    </Carousel.Caption>
  </Carousel.Item>
  <Carousel.Item>
    <img
      className="imgCarousel"
      src={Carousel2}
      alt="Second slide"
    />

    <Carousel.Caption>
      <h3 className="CaptionDescription">Amazon Echo (3ra Gen)</h3>
      <p  className="CaptionDescription"><b>$99.99</b></p>
    </Carousel.Caption>
  </Carousel.Item>
  <Carousel.Item>
    <img
      className="imgCarousel"
      src={Carousel3}
      alt="Third slide"
    />

    <Carousel.Caption>
      <h3 className="CaptionDescription">Amazon Echo Show 5 (3ra Gen)</h3>
      <p  className="CaptionDescription"><b>$89.99</b></p>
    </Carousel.Caption>
  </Carousel.Item>
</Carousel>
  
            
          </div>
       );
    }
 }

 export default Carrusel;