import React from 'react';
import Carrusel from './Carrusel';
import { Container, Row, Col } from 'react-bootstrap';
import img1 from '../img/echoDot3raGen_GrisOsc.jpg';
import img2 from '../img/echo3raGen_Carbon.jpg';
import img3 from '../img/echoShow5_3raGen_Carbon.jpg';
class MainContent extends React.Component {

    render() {
        return (
            <div>

                <Carrusel />
                <Container>
                    <Row>
                        <Col md={4}>
                        <img src={img1} style={{width:300,height:300}} alt="Echo Dot 3ra Generacion"/>
                        <p> Amazon Echo Dot (3ra Gen)
                        <br/><b>$49.99</b>
                        </p>
                        </Col>
                        <Col md={4}>
                        <img src={img2} style={{width:300,height:300}} alt="Echo Dot 3ra Generacion"/>
                        <p> Amazon Echo (3ra Gen)
                        <br/><b>$99.99</b>
                        </p>
                        </Col>
                        <Col md={4}>
                        <img src={img3} style={{width:300,height:300}} alt="Echo Dot 3ra Generacion"/>
                        <p> Amazon Echo Show 5(3ra Gen)
                        <br/><b>$89.99</b>
                        </p>
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }

}

export default MainContent;